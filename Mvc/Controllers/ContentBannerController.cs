﻿namespace SitefinityWebApp.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Web.Mvc;
    using Newtonsoft;
    using Newtonsoft.Json;
    using SitefinityWebApp.Mvc.ViewModels;
    using Telerik.Sitefinity.Data.Linq;
    using Telerik.Sitefinity.Libraries.Model;
    using Telerik.Sitefinity.Modules.Libraries;
    using Telerik.Sitefinity.Mvc;

    /// <summary>
    /// A content banner widget, used to display different static content.
    /// </summary>
    [ControllerToolboxItem(Name = "ContentBanner", Title = "ContentBanner", CssClass = "", SectionName = "Section Name")]
    public class ContentBannerController : Controller
    {
        /// <summary>
        /// The default list view template name.
        /// </summary>
        private string listTemplateName = "Default";

        /// <summary>
        /// The list prefix. All views (for list) should be prefixed with this prefix in order to be found by the controller.
        /// </summary>
        private string listTemplateNamePrefix = "Banner.";

        /// <summary>
        /// Gets or sets the title of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the theme title of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string ThemeTitle { get; set; }

        /// <summary>
        /// Gets or sets the summary of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the link name of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string LinkName { get; set; }

        /// <summary>
        /// Gets or sets the link url of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string LinkUrl { get; set; }

        /// <summary>
        /// Gets or sets the id of the content banner image.
        /// </summary>
        [Category("ImageProperties")]
        public string ImageId { get; set; }

        /// <summary>
        /// Gets or sets the video url of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string VideoUrl { get; set; }

        /// <summary>
        /// Gets or sets the muted title of the content banner widget.
        /// </summary>
        [Category("Properties")]
        public string TitleMuted { get; set; }

        /// <summary>
        /// Gets or sets the videos to use in the content banner.
        /// </summary>
        [Category("VideoProperties")]
        public Guid? Video { get; set; }

        /// <summary>
        /// The default value for UseBackgroundVideo property.
        /// </summary>
        private string useBackgroundVideo = "true";

        /// <summary>
        /// Gets or sets whether the web videos will be used.
        /// </summary>
        [Category("Properties")]
        public string UseBackgroundVideo
        {
            get
            {
                return this.useBackgroundVideo;
            }

            set
            {
                this.useBackgroundVideo = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the template that will be displayed.
        /// </summary>
        /// <value>The name of the list template</value>
        public string ListTemplateName
        {
            get
            {
                return this.listTemplateName;
            }

            set
            {
                this.listTemplateName = value;
            }
        }

        /// <summary>
        /// Returns the default action.
        /// </summary>
        /// <returns>The default action</returns>
        [HttpGet]
        public ActionResult Index()
        {
            var fullTemplateName = this.listTemplateNamePrefix + this.ListTemplateName;

            var viewModel = new ContentBannerViewModel
            {
                Title = this.Title,
                ThemeTitle = this.ThemeTitle,
                Summary = this.Summary,
                LinkName = this.LinkName,
                LinkUrl = this.LinkUrl,
                Image = this.GetImage(this.ImageId),
                VideoUrl = this.VideoUrl,
                TitleMuted = this.TitleMuted,
                Video = (!this.Video.HasValue || this.Video == Guid.Empty) ? null : LibrariesManager.GetManager().GetVideo(this.Video.Value),
                UseBackgroundVideo = this.UseBackgroundVideo
            };

            return this.View(fullTemplateName, viewModel);
        }

        /// <summary>
        /// This method serves as a safety net when other controllers are invoked and none of the actions match.
        /// </summary>
        /// <param name="actionName">The name of the action being invoked by other controllers.</param>
        protected override void HandleUnknownAction(string actionName)
        {
            this.ActionInvoker.InvokeAction(this.ControllerContext, "Index");
        }

        /// <summary>
        /// Returns image by its id.
        /// </summary>
        /// <param name="imageId">The id of the image to return</param>
        /// <returns>The image if an image with this id is found, null otherwise.</returns>
        private Image GetImage(string imageId)
        {
            if (string.IsNullOrWhiteSpace(imageId))
            {
                return null;
            }

            var librariesManager = LibrariesManager.GetManager();
            var image = librariesManager.GetImages().FirstOrDefault(im =>
                        im.Id == new Guid(imageId) &&
                        im.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live &&
                        im.Visible);

            return image;
        }        /// <summary>
                 /// Gets the live video by id.
                 /// </summary>
                 /// <param name="videoID">The video id.</param>
                 /// <returns>The video with the provided id if found, null otherwise.</returns>
        private Video GetVideo(Guid? videoID)
        {
            if (videoID != Guid.Empty)
            {
                LibrariesManager librariesManager = LibrariesManager.GetManager();
                var selectedVideoDataItem = librariesManager.GetVideos().FirstOrDefault(video =>
                        video.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live &&
                        video.Visible &&
                        video.Id == videoID);

                if (selectedVideoDataItem != null)
                {
                    return selectedVideoDataItem;
                }
            }

            return null;
        }

        /// <summary>
        /// Class used for deserialization of selected video ids
        /// </summary>
        private class VideoItem
        {
            /// <summary>
            /// Gets or sets the id of the item.
            /// </summary>
            public Guid Id { get; set; }
        }
    }
}