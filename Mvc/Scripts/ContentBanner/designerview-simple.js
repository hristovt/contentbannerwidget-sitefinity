﻿var designerModule = angular.module('designer');
angular.module('designer').requires.push('sfFields');
angular.module('designer').requires.push('sfSelectors');
angular.module('designer').requires.push('ngSanitize');

designerModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
    $scope.feedback.showLoadingIndicator = true;
    $scope.customProps = { videoItems: null };
    $scope.showField = function (field) {
        var map = {
            Default: {
                Title: true, ThemeTitle: false, Summary: false, LinkName: false, LinkUrl: false, Image: false, VideoUrl: false, TitleMuted: false, Video: false
            },
            Simple: {
                Title: true, ThemeTitle: true, Summary: false, LinkName: false, LinkUrl: false, Image: false, VideoUrl: false, TitleMuted: false, Video: false
            },
            SectionName: {
                Title: true, ThemeTitle: true, Summary: true, LinkName: false, LinkUrl: false, Image: false, VideoUrl: false, TitleMuted: false, Video: false
            },
            MainBanner: {
                Title: true, ThemeTitle: true, Summary: true, LinkName: true, LinkUrl: true, Image: true, VideoUrl: false, TitleMuted: false, Video: true
            },
            SimpleWithSummary: {
                Title: true, ThemeTitle: true, Summary: true, LinkName: false, LinkUrl: false, Image: false, VideoUrl: false, TitleMuted: false, Video: false
            },
            VideoBanner: {
                Title: true, ThemeTitle: true, Summary: true, LinkName: true, LinkUrl: true, Image: true, VideoUrl: true, TitleMuted: true, Video: false
            }
        };
        var view = map[$scope.properties.ListTemplateName.PropertyValue];
        return view ? view[field] : false;
    };

    $scope.resetPropertiesValues = function () {
        $.each($scope.properties, function (name, value) {
            if (value) {
                if (value.CategoryName == "Properties")
                    value.PropertyValue = "";
                else if (value.CategoryName == "ImageProperties") {
                    value.PropertyValue = "";
                    $scope.properties.selectedImage = null;
                    $('[sf-model="properties.' + name + '.PropertyValue"] [ng-show="sfImageIsVisible"]').attr("src", "");
                }
                else if (value.CategoryName == "VideoProperties") {
                    $scope.customProps.videoItems = [];
                }
            }
        });
    };

    propertyService.get()
        .then(function (data) {
            if (data) {
                $scope.properties = propertyService.toAssociativeArray(data.Items);
            }
        },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
        .finally(function () {
            $scope.feedback.showLoadingIndicator = false;
        });
}]);
