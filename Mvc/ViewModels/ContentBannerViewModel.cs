﻿namespace SitefinityWebApp.Mvc.ViewModels
{
    using System.Collections.Generic;
    using Telerik.Sitefinity.Libraries.Model;

    /// <summary>
    /// View model for the content banner widget.
    /// </summary>
    public class ContentBannerViewModel
    {
        /// <summary>
        /// Gets or sets the title for the content banner.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the theme title for the content banner.
        /// </summary>
        public string ThemeTitle { get; set; }

        /// <summary>
        /// Gets or sets the summary of the content banner widget.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the link name of the content banner widget.
        /// </summary>
        public string LinkName { get; set; }

        /// <summary>
        /// Gets or sets the link url of the content banner widget.
        /// </summary>
        public string LinkUrl { get; set; }

        /// <summary>
        /// Gets or sets the image for the content banner widget.
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// Gets or sets the Video url of the content banner widget.
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// Gets or sets the muted title of the content banner widget.
        /// </summary>
        public string TitleMuted { get; set; }

        /// <summary>
        /// Gets the videos for the content banner widget.
        /// </summary>
        public Video Video { get; internal set; }

        /// <summary>
        /// Gets or sets the value indicating whether to use videos.
        /// </summary>
        public string UseBackgroundVideo { get; set; }
    }
}